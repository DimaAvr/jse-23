package ru.tsc.avramenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.repository.IProjectRepository;
import ru.tsc.avramenko.tm.api.repository.ITaskRepository;
import ru.tsc.avramenko.tm.api.service.IProjectTaskService;
import ru.tsc.avramenko.tm.exception.empty.EmptyIdException;
import ru.tsc.avramenko.tm.exception.empty.EmptyIndexException;
import ru.tsc.avramenko.tm.exception.empty.EmptyNameException;
import ru.tsc.avramenko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.avramenko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    private final IProjectRepository projectRepository;

    public ProjectTaskService(ITaskRepository taskRepository, IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @NotNull
    @Override
    public List<Task> findTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        return taskRepository.findAllTaskByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    public Task bindTaskById(@NotNull final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        if (!taskRepository.existsById(userId, taskId)) throw new TaskNotFoundException();
        return taskRepository.bindTaskToProjectById(userId, projectId, taskId);
    }

    @NotNull
    @Override
    public Task unbindTaskById(@NotNull final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        if (!taskRepository.existsById(userId, taskId)) throw new TaskNotFoundException();
        return taskRepository.unbindTaskById(userId, taskId);
    }

    @NotNull
    @Override
    public Project removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        taskRepository.unbindAllTaskByProjectId(userId, projectId);
        return projectRepository.removeById(projectId);
    }

    @Nullable
    @Override
    public Project removeProjectByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @Nullable
        final Project project = projectRepository.findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        taskRepository.unbindAllTaskByProjectId(userId, project.getId());
        return projectRepository.removeByIndex(userId, index);
    }

    @Nullable
    @Override
    public Project removeProjectByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable
        final Project project = projectRepository.findByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        taskRepository.unbindAllTaskByProjectId(userId, project.getId());
        return projectRepository.removeByName(userId, name);
    }

}