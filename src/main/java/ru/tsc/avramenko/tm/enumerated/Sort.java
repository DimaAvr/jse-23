package ru.tsc.avramenko.tm.enumerated;

import ru.tsc.avramenko.tm.comparator.ComparatorByCreated;
import ru.tsc.avramenko.tm.comparator.ComparatorByName;
import ru.tsc.avramenko.tm.comparator.ComparatorByStartDate;
import ru.tsc.avramenko.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {

    NAME("Sort by name.", ComparatorByName.getInstance()),
    CREATED("Sort by created.", ComparatorByCreated.getInstance()),
    START_DATE("Sort by start date.", ComparatorByStartDate.getInstance()),
    STATUS("Sort by status.", ComparatorByStatus.getInstance());

    private final String displayName;

    private final Comparator comparator;

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

}
